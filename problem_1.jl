### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ d71a4620-b441-11ec-304c-5542253bf207
using CSV,DataFrames,Flux, Plots, Statistics,MLBase,Parameters,CUDA,MLDatasets, Lathe

# ╔═╡ 819b8378-5c51-4a74-b3d1-fdba305d1105
begin
	using Flux: train!
	using Flux.Data: DataLoader
	using Flux: onehotbatch, onecold, logitcrossentropy, throttle, @epochs
	using Base.Iterators: repeated
	using Lathe.preprocess: TrainTestSplit
end

# ╔═╡ 2914d3d8-a4a5-4ca6-adc5-e6091401a056
df = CSV.read("C:\Users\Lasarus\Downloads/real_estate.csv",DataFrame)

# ╔═╡ 88705eb1-dcb8-4638-b22b-632c28b6db1f
md"### RENAME COLUMN NAMES"

# ╔═╡ e680fdba-57fa-423e-b24b-aeea3086d02a
# Fix column names by replaceing ' ', '-', '/' with '_'
begin
	# Fix column names by replaceing ' ', '-', '/' with '_'
colnames = Symbol[]
for i in string.(names(df))
    push!(colnames,Symbol(replace(replace(replace(strip(i)," " => "_"),"-" => "_"), "/" => "_")))
end

rename!(df, colnames);
end

# ╔═╡ 111dac96-879c-4c31-ac1f-84370f36def0
names(df)

# ╔═╡ 98c871aa-d7d2-4594-baa3-3201adc448b1
cor(Matrix(df))

# ╔═╡ 50662f53-b27a-4fd3-a659-77cc744d89de
begin
	x = df[!,3:7]
	xMatrix = Matrix(x)
end

# ╔═╡ 3612f33b-5a4a-4c99-acc8-dab3c113f275
begin
	HousePriceUnitArea = df[!,end]
	yMatrix = Vector(HousePriceUnitArea)
end

# ╔═╡ 51f54483-77f9-4190-a43a-7cee58caa865
TrainingSize = 0.7

# ╔═╡ 9492a43e-f5db-407b-a67a-3da1bcef58eb
AllDataSize = size(yMatrix)[1]

# ╔═╡ ae18d7bf-1bcd-4c69-8091-29148efe66f7
TrainingIndex = trunc(Int64, TrainingSize * AllDataSize)

# ╔═╡ ced02c8d-b15f-4f43-adb7-f02c9c4a0447
xTrain = xMatrix[1: TrainingIndex, :]

# ╔═╡ fd9cfd9e-38db-4c99-868a-aa57d4c0e4de
xTest = xMatrix[TrainingIndex + 1: end,: ]

# ╔═╡ 910634a5-709f-4f52-84aa-15709d5e895f
yTrain = yMatrix[1: TrainingIndex]

# ╔═╡ 3a64b7a5-9894-4347-b7c7-dde5f1975dc2
yTest = yMatrix[TrainingIndex + 1: end]

# ╔═╡ 24780716-fabd-490f-9819-75a851f0774e
onehotbatch(yTrain, 0:5)

# ╔═╡ 88993b3a-d62c-469c-86a9-695d88dbf70a
md"#"

# ╔═╡ 325e729a-9b10-409e-9f17-ea6bea17085e
md"# Loss Function"

# ╔═╡ 6eddec75-c2ec-4ebd-8930-c29c5330e965
model = Chain(Dense(1, 1, σ))

# ╔═╡ 65b568bf-9936-490d-8dc8-d59ec55eaa28
model.weight

# ╔═╡ 827c9e23-81af-4db9-adf8-114ed40ca20a
model.bias

# ╔═╡ 81d16493-0380-40e1-954c-a7f1871b83e1
opt = Descent()

# ╔═╡ 933ba637-5d99-4ff7-ad46-4bcf9cb7ea5f
loss(x, y) = Flux.Losses.mse(model(x), y)

# ╔═╡ 197093bd-1876-402a-9a7d-2c859672cb02
md"#"

# ╔═╡ b173714a-6469-4375-8d44-19d90cd4e57c
loss(xTrain, yTrain)

# ╔═╡ 7bdbdfe3-a116-4ac9-a186-7f112ca2768e
data = [(xTrain, yTrain)]

# ╔═╡ 6e274ec7-8277-4879-bd28-7284f8297aaf
parameters = Flux.params(model)

# ╔═╡ 9000549f-a63e-4842-bd42-ace25b08c781
train!(loss, parameters, data, opt)

# ╔═╡ 6a1b1e37-e7f4-4300-b761-b7c6ad5b0159
loss(xTest, yTest)

# ╔═╡ 9a520a6b-87a9-42ed-8b59-d6410782234e
md"# PLOTTING"

# ╔═╡ Cell order:
# ╠═d71a4620-b441-11ec-304c-5542253bf207
# ╠═819b8378-5c51-4a74-b3d1-fdba305d1105
# ╠═2914d3d8-a4a5-4ca6-adc5-e6091401a056
# ╠═88705eb1-dcb8-4638-b22b-632c28b6db1f
# ╠═e680fdba-57fa-423e-b24b-aeea3086d02a
# ╠═111dac96-879c-4c31-ac1f-84370f36def0
# ╠═98c871aa-d7d2-4594-baa3-3201adc448b1
# ╠═50662f53-b27a-4fd3-a659-77cc744d89de
# ╠═3612f33b-5a4a-4c99-acc8-dab3c113f275
# ╠═51f54483-77f9-4190-a43a-7cee58caa865
# ╠═9492a43e-f5db-407b-a67a-3da1bcef58eb
# ╠═ae18d7bf-1bcd-4c69-8091-29148efe66f7
# ╠═ced02c8d-b15f-4f43-adb7-f02c9c4a0447
# ╠═fd9cfd9e-38db-4c99-868a-aa57d4c0e4de
# ╠═910634a5-709f-4f52-84aa-15709d5e895f
# ╠═3a64b7a5-9894-4347-b7c7-dde5f1975dc2
# ╠═24780716-fabd-490f-9819-75a851f0774e
# ╠═88993b3a-d62c-469c-86a9-695d88dbf70a
# ╠═325e729a-9b10-409e-9f17-ea6bea17085e
# ╠═6eddec75-c2ec-4ebd-8930-c29c5330e965
# ╠═65b568bf-9936-490d-8dc8-d59ec55eaa28
# ╠═827c9e23-81af-4db9-adf8-114ed40ca20a
# ╠═81d16493-0380-40e1-954c-a7f1871b83e1
# ╠═933ba637-5d99-4ff7-ad46-4bcf9cb7ea5f
# ╠═197093bd-1876-402a-9a7d-2c859672cb02
# ╠═b173714a-6469-4375-8d44-19d90cd4e57c
# ╠═7bdbdfe3-a116-4ac9-a186-7f112ca2768e
# ╠═6e274ec7-8277-4879-bd28-7284f8297aaf
# ╠═9000549f-a63e-4842-bd42-ace25b08c781
# ╠═6a1b1e37-e7f4-4300-b761-b7c6ad5b0159
# ╠═9a520a6b-87a9-42ed-8b59-d6410782234e
