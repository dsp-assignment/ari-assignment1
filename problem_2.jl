### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ bbaa72a0-b461-11ec-239a-ade81bb34a72
using Pkg

# ╔═╡ 25f1e5fa-b8f4-4068-9d96-bdef36f7e4c2
begin
using Flux, Statistics
using Flux: onehotbatch, onecold, crossentropy, throttle
using Base.Iterators: repeated, partition
using BSON
using ImageView
using Printf	
end

# ╔═╡ c5c03a35-dc33-4cdd-a6f3-5e1bff758e60
Pkg.activate("Project.toml")

# ╔═╡ d78ab5cf-f2d5-4395-b452-43194186e658
function load_data(path)
	x=readdir(path,join=true)
	return x
end

# ╔═╡ 51e55e0c-24ac-4a62-a054-c933661eb376
begin
train_label = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\train")
test_label = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\test")
train_normal = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\train\\NORMAL")
train_pneumonia = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\train\\PNEUMONIA")
test_normal = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\test\\NORMAL")
test_pneumonia = load_data("C:\\Users\\lasarus\\Desktop\\chest_xray\\test\\PNEUMONIA")
end

# ╔═╡ 64736d9f-0240-4bdf-8c97-8d28fa1eea8d
function AlexNet_Architecture(; imgsize=(227,227,3), nclasses=1000) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 256)
    
    return Chain( 
		    Conv((11,11), imgsize[end]=>96, relu), MaxPool((3,3)),
            Conv((5,5), 96 => 256, relu), MaxPool((3,3)),
            Conv((3,3), 256 => 384, relu),
            Conv((3,3), 384 => 384, relu),
            Conv((3,3), 384 => 256, relu), MaxPool((3,3)),
            flatten,
            Dense(prod(out_conv_size), 4096, relu),
            Dense(4096, 4096, relu),
            Dense(4096, 1000)
           )
end 

# ╔═╡ f2d27ed1-b4f9-4ae8-ad61-8b233e6e33ce
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = AlexNet_Architecture(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ 896d2951-bde4-4121-a5a3-3b5be03c888e
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ 2f8b8f32-241a-4a32-b9c2-f3b4eed50cbd
opt = ADAM(0.001);

# ╔═╡ b1ef0a3d-7aad-430a-b777-6ea0d860f7fe
best_acc = 0.0

# ╔═╡ 61c8b9a9-c318-4c1c-8714-195116b0edff
last_improvement = 0

# ╔═╡ beab88a3-672a-4be8-845d-6465b200ee80
accuracy_target = 0.97

# ╔═╡ 309511ec-b240-49cb-ae1b-b18efe513fa5
max_epochs = 100

# ╔═╡ d7269945-9a85-4f0c-82d9-59bbcb232329
for epoch_idx in 1:100
    global best_acc, last_improvement
    Flux.train!(loss, params(AlexNet_Architecture), train_normal, opt)

    
    acc = accuracy(train_normal...)
    @info(@sprintf("[%d]: Train accuracy: %.4f", epoch_idx, acc))
    
    
    acc = accuracy(test_normal...)
    @info(@sprintf("[%d]: Test accuracy: %.4f", epoch_idx, acc))

    if acc >= accuracy_target
        @info(" -> Early-exiting: We reached our target accuracy of $(accuracy_target*100)%")
        break
    end

    if epoch_idx - last_improvement >= 10
        @warn(" -> We're calling this converged.")
        break
    end
end

# ╔═╡ Cell order:
# ╠═bbaa72a0-b461-11ec-239a-ade81bb34a72
# ╠═c5c03a35-dc33-4cdd-a6f3-5e1bff758e60
# ╠═25f1e5fa-b8f4-4068-9d96-bdef36f7e4c2
# ╠═d78ab5cf-f2d5-4395-b452-43194186e658
# ╠═51e55e0c-24ac-4a62-a054-c933661eb376
# ╠═64736d9f-0240-4bdf-8c97-8d28fa1eea8d
# ╠═f2d27ed1-b4f9-4ae8-ad61-8b233e6e33ce
# ╠═896d2951-bde4-4121-a5a3-3b5be03c888e
# ╠═2f8b8f32-241a-4a32-b9c2-f3b4eed50cbd
# ╠═b1ef0a3d-7aad-430a-b777-6ea0d860f7fe
# ╠═61c8b9a9-c318-4c1c-8714-195116b0edff
# ╠═beab88a3-672a-4be8-845d-6465b200ee80
# ╠═309511ec-b240-49cb-ae1b-b18efe513fa5
# ╠═d7269945-9a85-4f0c-82d9-59bbcb232329
